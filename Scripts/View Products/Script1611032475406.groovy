import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'Maximize current window'
WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.setText(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    'standard_user')

WebUI.setEncryptedText(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    'qcu24s4901FyWDTwXGr6XA==')

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Test.allTheThings() T-Shirt (Red)This c_a99f8d'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Test.allTheThings() T-Shirt (Red)'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/button_- Back'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/button_ADD TO CART'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Sauce Labs Bolt T-ShirtGet your testing_76f43a'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Sauce Labs Bolt T-ShirtGet your testing_096930'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/button_ADD TO CART'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Sauce Labs Fleece JacketIts not every d_f43511'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Sauce Labs Fleece JacketIts not every d_e9a1b2'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Its not every day that you come across _897e84'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/button_ADD TO CART'))

WebUI.selectOptionByValue(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'za', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'lohi', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'hilo', true)

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/div_Open Menu_header_label'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/button_Open Menu'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/Page_Swag Labs/a_Logout'))

'Close browser'
WebUI.closeBrowser()


