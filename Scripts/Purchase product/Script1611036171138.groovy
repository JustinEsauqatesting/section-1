import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'Maximize current window'
WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Accepted usernames are                s_40dcc1'))

WebUI.setText(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    'standard_user')

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Password for all users                s_477655'))

WebUI.setEncryptedText(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    'qcu24s4901FyWDTwXGr6XA==')

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Sauce Labs Bike LightA red light isnt t_13f308'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_A red light isnt the desired state in t_cf0f76'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Sauce Labs Bike Light'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/button_ADD TO CART'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Open Menu_shopping_cart_container'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Sauce Labs Bike LightA red light isnt t_4173d7'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Continue ShoppingCHECKOUT'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/a_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_Checkout Your Information_checkout_info'))

WebUI.setText(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_Checkout Your Information_first-name'), 
    'John')

WebUI.setText(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_Checkout Your Information_last-name'), 
    'Doe')

WebUI.setText(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_Checkout Your Information_postal-code'), 
    '8001')

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_CANCEL'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/input_CANCEL_btn_primary cart_button'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_1Sauce Labs Bike LightA red light isnt _476e59'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_FREE PONY EXPRESS DELIVERY'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_CANCELFINISH'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/a_FINISH'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/h2_THANK YOU FOR YOUR ORDER'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/div_THANK YOU FOR YOUR ORDER            You_8cbee8'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/h2_THANK YOU FOR YOUR ORDER'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/button_Open Menu'))

WebUI.click(findTestObject('Object Repository/TestCase_Viewproduct/check out/Page_Swag Labs/a_Logout'))

'Close browser'
WebUI.closeBrowser()

