import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'Maximize current window'
WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    username_1)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    password)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/button_Open Menu'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/a_Logout'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    username_2)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    password)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/button_Open Menu'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/a_Logout'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    username_3)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    password)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/button_Open Menu'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/a_Logout'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 
    username_4)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TestCase-one/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'), 
    password)

WebUI.click(findTestObject('Object Repository/TestCase_login_validation/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))

WebUI.verifyElementText(findTestObject('Object Repository/TestCase_login_validation/Page_Swag Labs/h3_Epic sadface Sorry, this user has been l_a91fd7'), 
    'Epic sadface: Sorry, this user has been locked out.')

WebUI.delay(2)

'Close browser'
WebUI.closeBrowser()

