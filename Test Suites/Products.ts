<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Products</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>78d6208a-ada7-4bde-9799-74ef1a2fbf79</testSuiteGuid>
   <testCaseLink>
      <guid>d7a819a3-5b02-46c4-ae3a-a007c3d7c40e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/View Products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4a3a806-dfd4-4eb0-a184-87aa3d293bed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Purchase product</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
