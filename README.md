#Author: esau.justin1@gmail.com
#Name: Justin Esau
#Navigate to Scripts to view Test Cases and Code
#Features: 	List of scenarios.
			List of Test Cases / Test Scripts
			List of Test Suite and Test Suite Collection
			
			
Test Cases / Test Scripts			
	Login Valid
	Login InValid
	Variable Login and Logout Test
	Login and Navigate
	View Products
	Purchase product

#Feature Definition
@Login
Feature: Login Feature

Test Case: Login Valid
  
  As a user, I want to login to SwagLabs
  so that I can view products.

  @Valid
  Scenario Outline: Login with a valid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter username <username> and password <password>
    And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 

    Examples: 
      | username 	  | password     |
      | standard_user | secret_sauce |

Test Case: Login Invalid

	As a user, I want to login to SwagLabs with an invalid credential.

  @InValid
  Scenario Outline: Login with an invalid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter an invalid username <username> and password <password>
    And I click Log in button
	Then I should NOT be able to login successfully
	And Login Valadation should kick in with error message displaying

    Examples: 
      | username        | password           |
      | locked_out_user | secret_sauce       |
      | Jane Doe 	    | ThisIsNotAPassword |


Test Case: Variable Login and Logout Test
  
  As a user, I want to login to SwagLabs
  with all available Credentials as local Variables and Logout.

  @Valid & InValid
  Scenario Outline: Login with a valid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter username <standard_user> and password <secret_sauce> as a local variable
    And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	Then I click Menu
	And I click Logout
	Then I enter username <problem_user> and password <secret_sauce> as a local variable
	And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	Then I click Menu
	And I click Logout
	Then I enter username <performance_glitch_user> and password <secret_sauce> as a local variable
	And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	Then I click Menu
	And I click Logout
	Then I enter username <locked_out_user> and password <secret_sauce> as a local variable
	And I click Log in button 
	Then I should NOT be able to login successfully
	And Login Valadation should kick in with error message displaying

    Examples: 
      | username 	  			| password     |
      | standard_user 			| secret_sauce | 
      | problem_user 			| secret_sauce |
      | performance_glitch_user | secret_sauce |
      | locked_out_user 		| secret_sauce |     


#Feature Definition
@Navigate
Feature: Login Navigate and Logout Feature      
Test Case: Login and Navigate

	As a user, I want to login to SwagLabs
  	navigate around and logout.

   Scenario Outline: Login with a valid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter username <username> and password <password>
    And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	And I view products page
	Then I open the menu 
	And click Logout button
	Then I am directed to the Login page

    Examples: 
      | username 	  | password     |
      | standard_user | secret_sauce |





#Feature Definition
@Product
Feature: Login Navigate View Product and Logout Feature      
Test Case: View Products

	As a user, I want to login to SwagLabs
  	so that I can view products and logout.

   Scenario Outline: Login with a valid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter username <username> and password <password>
    And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	And I view products page
	Then I select a product
	And I am directed to product item
	And I Click Add to Cart 
	Then I Click Back Button
	Then I am directed back to Products page
	Then I select filter items
	This then changes the Products view according to filter change
	Then I open the menu 
	And click Logout button
	Then I am directed to the Login page

    Examples: 
      | username 	  | password     |
      | standard_user | secret_sauce |     


#Feature Definition
@Product
Feature: Login Navigate View Product Purchase Product Checkout and Logout Feature      
Test Case: Purchase product

	As a user, I want to login to SwagLabs
  	so that I can view products and purchase 
  	product and go through checkoout process and logout.

   Scenario Outline: Login with a valid credential
    Given I navigate to SwagLabs Login page
    When I add my credentials
    And I enter username <username> and password <password>
    And I click Log in button 
	Then I should be able to login successfully
	And direct user to Products page 
	And I view products page
	Then I select a product
	And I am directed to product item
	And I click Add to Cart 
	Then I click Cart Icon
	Then I am directed back to Your Cart page
	Then I click Checkout button
	This then takes User to Your Information page
	Then I add First Name <firstname > Last Name <lastname> and Zip/Postal Code <zipcode>
	And I click Continue button
	Then I am directed to Overiew page
	And I View my purchase information
	And I click Finish button
	Then I am presented with "THANK YOU FOR YOUR ORDER"
	Then I open the menu 
	And click Logout button
	Then I am directed to the Login page

    Examples: 
      | username 	  | password     |
      | standard_user | secret_sauce |   

    Examples: 
      | firstname 	  | lastname    | zipcode |
      | John		  | Doe 		| 8001	  |             




|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

List of Test Suite and Test Suite Collection

Test Suite = 2  | Test Suite Collection = 1

Login
	Login Valid
	Login InValid
	Variable Login and Logout Test
	
Products
	View Products
	Purchase product



